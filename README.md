# wcaptd

> Capture DNS requests and discover ads and tracking servers based on domain name, CNAME alias, or regex pattern.

## Introduction

`wcaptd` can be used for building ad blocking lists. It allows to detect subdomains of already flagged domains, which is useful for hosts file format (since it doesn't support wildcards). It can also detect [CNAME-cloacked domains](https://github.com/AdguardTeam/cname-trackers#cname-cloaked-trackers) and match unwanted domains using regular expressions (regex).

To run, it requires a database that already contains a few flagged domains. If you wish to use a production-ready database, you can use the one from [TBlock](https://tblock.me) that can be [downloaded here](http://download.tblock.me/pub/adservers.db.xz).

**Note:** root access is needed in order to run `wcaptd`.

## Usage

```text
$ ./wcaptd -h                
usage: ./wcaptd [--help|--start|--export <num>] [database.db]

options:
 -h --help          show this help
 -s --start         start logging dns request
 -x --export <conf> export domains stored in database
 -c --export-csv    export database as csv

<conf> (confidence level):
 1                  high (reviewed)
 2                  medium (subdomain/CNAME)
 3                  low (regex)
 0                  false positive
```

## License

[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see \<https://www.gnu.org/licenses/\>.
